+++
title = "My Favorite Designer"
date = 2021-07-17T03:12:50Z
author = "Marziyeh shahsavaran"
description = "Santiago Calatrava."
+++

# My favorite designer : Santiago Calatrava

Santiago Calatrava Valls (born 28 July 1951) is a Spanish architect, structural engineer, sculptor and painter, particularly known for his bridges supported by single leaning pylons, and his railway stations, stadiums, and museums, whose sculptural forms often resemble living organisms.His best-known works include the Olympic Sports Complex of Athens, the Milwaukee Art Museum, the Turning Torso tower in Malmö, Sweden, the World Trade Center Transportation Hub in New York City, the Auditorio de Tenerife in Santa Cruz de Tenerife, the Margaret Hunt Hill Bridge in Dallas, Texas, and his largest project, the City of Arts and Sciences and Opera House in his birthplace, Valencia. His architectural firm has offices in New York City, Doha, and Zürich.

## Calatrava's Projects of the 1990s

### 1. Palace of the Arts in Valencia (2006)

![https://gitlab.com/Marziyehsh/marzieh-shahsavarans-personal-website/-/raw/master/santiagoA.png](https://gitlab.com/Marziyehsh/marzieh-shahsavarans-personal-website/-/raw/master/santiagoA.png)

### 2. Liège-Guillemins railway station, Liège, Belgium (2009)

![https://gitlab.com/Marziyehsh/marzieh-shahsavarans-personal-website/-/raw/master/santiagoB.png](https://gitlab.com/Marziyehsh/marzieh-shahsavarans-personal-website/-/raw/master/santiagoB.png)

### 3. Milwaukee Art Museum in Milwaukee, Wisconsin, U.S. (2001)

![https://gitlab.com/Marziyehsh/marzieh-shahsavarans-personal-website/-/raw/master/santiagoC.png](https://gitlab.com/Marziyehsh/marzieh-shahsavarans-personal-website/-/raw/master/santiagoC.png)

### 4. Florida Polytechnic University in Lakeland, Florida, U.S. (2014)

![https://gitlab.com/Marziyehsh/marzieh-shahsavarans-personal-website/-/raw/master/santiagoD.png](https://gitlab.com/Marziyehsh/marzieh-shahsavarans-personal-website/-/raw/master/santiagoD.png)

