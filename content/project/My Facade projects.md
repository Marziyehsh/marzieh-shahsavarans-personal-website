+++
title = "My Facade projects"
description = "Description of my projects."
date = 2021-07-17T03:12:50Z
author = "Marziyeh shahsavaran"
+++



## About my projects

I have been working in Iran for about six years after completing my bachelor's degree. I worked for a design company and was responsible for designing the plans and facades of buildings.The design and drawing work of the plan was done with AutoCAD software and the design of the building facade was done with 3D Max software.


**1. Residential project, classic design style, located in Farmanieh St., Tehran**

![Facade-design.png](https://gitlab.com/Marziyehsh/marzieh-shahsavarans-personal-website/-/raw/master/Facade-design.png)

**2.Villa project in Lavasanat area of Tehran**

![my-project.png](https://gitlab.com/Marziyehsh/marzieh-shahsavarans-personal-website/-/raw/master/my-project.png)

**3.Classical and Roman design residential project in Andarzgoo area located in the north of Tehran**

![facade3.png](https://gitlab.com/Marziyehsh/marzieh-shahsavarans-personal-website/-/raw/master/facade3.png)



